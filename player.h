#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <vector>
#include "common.h"
#include "board.h"
using namespace std;

class Player {
    
private:
    Board *board;
    Side side;

public:
    Player(Side side);
    ~Player();
    Move* bestMove( vector< Move* > moves );
    Move* miniMax( Board * start, Side side, int depth);
    Move *doMove(Move *opponentsMove, int msLeft);
    vector< Move* > findMoves(); 
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

    void setBoard (Board* board);
};

#endif
