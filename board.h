#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <vector>
#include "common.h"
using namespace std;

int scoreSpot( int i, int j);

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;    
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    vector< Move* > findMoves( Side side );
    void doMove(Move *m, Side side);
    int scoreMove( Move *m, Side mover, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();

    int scoreSide( Side side);
    
    void setBoard(char data[]);
};


bool onEdge(int x, int y);
bool onCorner(int x, int y);
bool innerCorner( int x, int y);
bool nearCorner( int x, int y);

#define CORNER    200
#define NEAR           -2
#define EDGE           50
#define INNER         -1
#define NORMAL   1

#endif
