#include "board.h"
#include <stdio.h>

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Find all the possible moves for a side, and return a vector of them.
 *
 * If the vector's size is 0, then there are no valid moves.
 *
 * NOTE: This allocates memory to a new vector when called.
 */
  

vector< Move* > Board::findMoves(Side side)
{
    vector< Move* > moves;

    // Iterate through all the moves, accepting them if valid.
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            Move* new_move = new Move(x, y);
            if ( this->checkMove( new_move, side ) )
            {
                // If this is a valid move, according to our board, then add it to the vector.
                moves.push_back( new_move );
            }
            else
            {
                 // Otherwise, return the memory allocated and move on.
                 delete new_move;
            }
        } // END y-loop
    } // END x-loop
    
    return moves;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Scores a given move.
 *
 */
int Board::scoreMove( Move *m, Side mover, Side scorer )
{
    Board * copy = this->copy();
    copy->doMove( m,  mover );
    int score;
    int B = copy->scoreSide(BLACK);
    int W = copy->scoreSide(WHITE);
    score = (scorer == BLACK)? (B - W):(W - B);
    delete copy;
    return score;
}

int Board::scoreSide(Side side) 
{
    int score = 0;
    for (int i = 0; i < 8; i++) 
    {
        for (int j = 0; j < 8; j++) 
       {
            if ( this->get(side, i, j) )
            {
                score += scoreSpot( i, j);
            }
       }
    }
    return score;
}


/* 
 * Scores a specific spot. Essentially a heuristic metric.
 *
 */

int scoreSpot( int i, int j)
{ 
    if ( onEdge(i,j) )
    {
        if ( onCorner(i,j) )
        {
            return CORNER;
        }
        else if ( nearCorner(i,j) )
        {
            return NEAR;
        }
        else
        {
            return EDGE;
        }
    }
    else if ( innerCorner(i,j) )
    {
        return INNER;
    }
    else
    {
        return NORMAL;
    }
}

bool onEdge(int x, int y)
{
    return ( (x == 0)||( x == 7) || (y == 0) || ( y == 7 ) );
}

bool onCorner(int x, int y)
{
    return  ( (x == 0)||(x == 7) )&&( (y == 0)||(y == 7) );
}

bool nearCorner(int x, int y)
{
    bool left = ( ( x == 0 )&&( ( y == 1 )||( y == 6) ) );
    bool right = ( ( x == 7 )&&( ( y == 1 )||( y == 6 ) ) );
    bool top = ( ( y == 0 )&&( ( x == 1 )||( x == 6) ) );
    bool bottom = ( ( y == 7 )&&( ( x == 1 )||( x == 6) ) );
    return ( left || right || top || bottom );
}

bool innerCorner(int x, int y)
{
        return  ( (x == 1)||(x == 6) )&&( (y == 1)||(y == 6) );
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
