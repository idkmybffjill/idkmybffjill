#include "player.h"
#include <stdio.h>
#define DEPTH 5
// TODO: Everything. - Jalani

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     this->side = side;
     this->board = new Board();
}

/*
 * Destructor for the player.
 */
Player::~Player() 
{
    delete this->board;
}
void Player::setBoard( Board *board )
{
    this->board = board->copy();
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{  
    int depth;
    if ( this->testingMinimax )
    {
        depth = 2;
    }
    else
    {
        depth = DEPTH;
    }
     // Update the board.
     Side other = (side == BLACK) ? WHITE : BLACK;
     this->board->doMove( opponentsMove, other);

    // Now find available moves.
    vector< Move*> moves = this->board->findMoves(this->side);

    if (moves.size() == 0)
    {
        // If there are no valid moves...
        return NULL;
    }
    else
    {
        // Find the best move. (Or in this case choose the first move available)
        Move* choice = this->miniMax( this->board, this->side, depth );
        // Update the board before returning.
        this->board->doMove( choice, side);
        return choice;
    }
}



/*
 * Finds the best move from a given vector of them, destroying the vector in the process.
 *
 * TODO: make it real. Write a scoring function for the board based on heuristics and use it here.
 */

Move* Player::bestMove( vector< Move* > moves )
{
    int size =  moves.size();
    int* move_scores = new int[size];
    int champ = 0;
    // Choose the move with the highest score, by iterating through all the scores.
    for (int i = 0; i < size; i++)
    {
        move_scores[i] = this->board->scoreMove( moves[i], this->side, this->side );
        if ( move_scores[champ] <= move_scores[i] )
        {
            champ = i;
        }
        fprintf( stderr, "i:%d\n", i);
    }
    Move *choice = new Move( moves[champ]->getX(), moves[champ]->getY());
    moves.clear();
    delete[] move_scores;
    return choice;
}

Move* Player::miniMax( Board* start, Side side, int depth )
{
    Side other = ( side == BLACK )? (WHITE):(BLACK);
    vector< Move* > moves = start->findMoves( side );
    if (moves.size() == 0)
    {
        return NULL;
    }
    int* move_scores = new int[ moves.size() ];
    int champ = 0;

    // Choose the move with the highest score in a (depth -1) play
    for ( int i = 0; i < moves.size(); i++)
    {
        Board* work_board = start->copy();
        work_board->doMove( moves[i], side );

        // Enter recursion.
        if ( depth == 1 )
        {
            // Base case.
            move_scores[i] = work_board->scoreSide(side);
        }
        else
        {
            // Recursive call.
            int new_depth = depth - 1;
            Move* adverse = this->miniMax( work_board, other, new_depth );
            move_scores[i] = work_board->scoreMove( adverse, other, side);
            delete adverse;
        }
        delete work_board;

        if ( move_scores[champ] <= move_scores[i] )
        {
            champ = i;
        }
    }

    // Return our choice properly.
    Move *choice = new Move( moves[champ]->getX(), moves[champ]->getY());
    moves.clear();
    delete[] move_scores;
    return choice;    
}      
